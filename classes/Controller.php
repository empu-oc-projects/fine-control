<?php

namespace Empu\FineControl\Classes;

use Backend\Classes\Controller as OcController;
use Backend\Facades\Backend;
use Backend\Facades\BackendAuth;
use Backend\Models\BackendPreferences;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

/**
 * Controller
 */
class Controller extends OcController
{
    protected $actionPermissions = [];

    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the controller action.
     * @param string $action The action name.
     * @param array $params Routing parameters to pass to the action.
     * @return mixed The action result.
     */
    public function run($action = null, $params = [])
    {
        $this->action = $action;
        $this->params = $params;

        /*
         * Check security token.
         */
        if (!$this->verifyCsrfToken()) {
            return Response::make(Lang::get('backend::lang.page.invalid_token.label'), 403);
        }

        /*
         * Extensibility
         */
        if (
            ($event = $this->fireEvent('page.beforeDisplay', [$action, $params], true)) ||
            ($event = Event::fire('backend.page.beforeDisplay', [$this, $action, $params], true))
        ) {
            return $event;
        }

        /*
         * Determine if this request is a public action.
         */
        $isPublicAction = in_array($action, $this->publicActions);

        // Create a new instance of the admin user
        $this->user = BackendAuth::getUser();

        /*
         * Check that user is logged in and has permission to view this page
         */
        if (!$isPublicAction) {

            /*
             * Not logged in, redirect to login screen or show ajax error.
             */
            if (!BackendAuth::check()) {
                return Request::ajax()
                    ? Response::make(Lang::get('backend::lang.page.access_denied.label'), 403)
                    : Backend::redirectGuest('backend/auth');
            }

            /*
             * Check access groups against the action definition
             */
            if (
                $this->actionPermissions
                && isset($this->actionPermissions[$action])
                && ! $this->user->hasAnyAccess($this->actionPermissions[$action])
            ) {
                return Response::make(View::make('backend::access_denied'), 403);
            }

            /*
             * Check access groups against the page definition
             */
            if ($this->requiredPermissions && !$this->user->hasAnyAccess($this->requiredPermissions)) {
                return Response::make(View::make('backend::access_denied'), 403);
            }
        }

        /*
         * Set the admin preference locale
         */
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }
        elseif ($this->user && ($locale = BackendPreferences::get('locale'))) {
            Session::put('locale', $locale);
            App::setLocale($locale);
        }

        /*
         * Execute AJAX event
         */
        if ($ajaxResponse = $this->execAjaxHandlers()) {
            return $ajaxResponse;
        }

        /*
         * Execute postback handler
         */
        if (
            ($handler = post('_handler')) &&
            ($handlerResponse = $this->runAjaxHandler($handler)) &&
            $handlerResponse !== true
        ) {
            return $handlerResponse;
        }

        /*
         * Execute page action
         */
        $result = $this->execPageAction($action, $params);

        if (!is_string($result)) {
            return $result;
        }

        return Response::make($result, $this->statusCode);
    }
}
