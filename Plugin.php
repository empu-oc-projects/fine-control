<?php

namespace Empu\FineControl;

use Backend;
use Backend\Facades\BackendAuth;
use Empu\FineControl\Event as EventHandler;
use Illuminate\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use System\Classes\PluginBase;

/**
 * FineControl Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Fine Control',
            'description' => 'Kendali fungsi CMS yang lebih baik',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-thumbs-up'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'backend.manage_groups' => [
                'tab' => 'Fine Control',
                'label' => 'Mengelola group dan mengatur grup pengguna'
            ],
            'backend.manage_user_permissions' => [
                'tab' => 'Fine Control',
                'label' => 'Mengatur izin per pengguna'
            ],
        ];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register the access gate service.
        $this->app->singleton(GateContract::class, function ($app) {
            return new Gate($app, function () use ($app) {
                return BackendAuth::getUser();
            });
        });
    }

    public function boot()
    {
        if (App::runningInBackend()) {
            // Extend all backend form usage
            Event::listen('backend.form.extendFields', EventHandler\ExtendFormFieldsHandler::class);
            Event::listen('backend.page.beforeDisplay', EventHandler\BeforePageDisplayHandler::class);
            Event::listen('backend.list.extendQuery', EventHandler\ExtendListQueryHandler::class);
        }
    }

}
