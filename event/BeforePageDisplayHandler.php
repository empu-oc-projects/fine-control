<?php

namespace Empu\FineControl\Event;

use Backend\Facades\BackendAuth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Backend\Controllers\Users as UsersController;
use Backend\Controllers\UserGroups as UserGroupsController;

/**
 * BeforePageDisplayHandler
 */
class BeforePageDisplayHandler
{
    /**
     * summary
     */
    public function handle ($controller, $action, $params)
    {
        $user = BackendAuth::getUser();
        $controllerClass = get_class($controller);

        // hanya superuser yg bs edit default user & default group
        if (
            $user &&
            ! $user->isSuperUser() &&
            in_array($controllerClass, [UsersController::class, UserGroupsController::class]) &&
            $action == 'update' &&
            $params[0] == 1
        ) {
            return $this->accessDenied();
        }

        // hanya yg berhak yg dapat manage group
        if (
            $user &&
            ! $user->hasAccess('backend.manage_groups') &&
            $controllerClass == UserGroupsController::class
        ) {
            return $this->accessDenied();
        }
    }

    public function accessDenied()
    {
        return Response::make(View::make('backend::access_denied'), 403);
    }
}
